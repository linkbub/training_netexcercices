class Question
{
    constructor()
    {
        this.Id = 0;
        this.PrakritiValue = "V";
        this.VikritiValue = "V";
    }
}

function SendTest()
{
    var questions = new Array();

    for (var i = 1; i < 41; i++)
    {
        var questionPV = "Q" + i + "P_V";
        var questionPP = "Q" + i + "P_P";
        var questionPK = "Q" + i + "P_K";
        
        var questionVV = "Q" + i + "V_V";
        var questionVP = "Q" + i + "V_P";
        var questionVK = "Q" + i + "V_K";

        var questionPId = "Q" + i + "P";
        var questionVId = "Q" + i + "V";


        try
        {
            var questionP_Value = document.querySelector('input[name="'+ questionPId +'"]:checked').value;
            var questionV_Value = document.querySelector('input[name="'+ questionVId +'"]:checked').value;
            
            var question = new Question();
            question.Id = i;
            question.PrakritiValue = questionP_Value;
            question.VikritiValue = questionV_Value;

            questions.push(question);
        }
        catch(error)
        {
            alert("falta seleccionar las opciones de la pregunta " + i);
            return;
        }        
    }

    var totalPrakriti_V = 0;
    var totalPrakriti_P = 0;
    var totalPrakriti_K = 0;

    var totalVikriti_V = 0;
    var totalVikriti_P = 0;
    var totalVikriti_K = 0;

    for (var i = 0; i < questions.length; i++)
    {
        var question = questions[i];
        console.log("Pregunta :" + question.Id + " Prakriti:" + question.PrakritiValue + "    " + " Vikriti:" + question.VikritiValue);

        if (question.PrakritiValue == "V")
            totalPrakriti_V++;
        else if (question.PrakritiValue == "P")
            totalPrakriti_P++;
        else if (question.PrakritiValue == "K")
            totalPrakriti_K++;

        if (question.VikritiValue == "V")
            totalVikriti_V++;
        else if (question.VikritiValue == "P")
            totalVikriti_P++;
        else if (question.VikritiValue == "K")
            totalVikriti_K++;
    }

    var perc_PV = Math.round(totalPrakriti_V * 100 / questions.length);
    var perc_PP = Math.round(totalPrakriti_P * 100 / questions.length);
    var perc_PK = Math.round(totalPrakriti_K * 100 / questions.length);

    var perc_VV = Math.round(totalVikriti_V * 100 / questions.length);
    var perc_VP = Math.round(totalVikriti_P * 100 / questions.length);
    var perc_VK = Math.round(totalVikriti_K * 100 / questions.length);

    console.log();
    console.log("Tu Pakriti es: " + perc_PV + "% Vata " + perc_PP + "% Pitta " + perc_PK + "% Kapha");
    console.log("Tu Vikriti es: " + perc_VV + "% Vata " + perc_VP + "% Pitta " + perc_VK + "% Kapha");

    let body = "Tu Pakriti es: " + perc_PV + "% Vata " + perc_PP + "% Pitta " + perc_PK + "% Kapha.\n";
    body += "Tu Vikriti es: " + perc_VV + "% Vata " + perc_VP + "% Pitta " + perc_VK + "% Kapha";

    window.open('mailto:info@ayurvedaimmersion.com?subject=dosha_test&from=' + TbEmail.value + '&body='+body);
}