using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Activitat1
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] arrayNumeros = { 2, 6, 8, 4, 5, 5, 9, 2, 1, 8, 7, 5, 9, 6, 4 };

            var mitjana = arrayNumeros.Average();
            var maxima = arrayNumeros.Max();
            var minima = arrayNumeros.Min();
            Console.Write(mitjana);
            Console.Write(minima);
            Console.Write(maxima);
            Console.Read();
        }
    }
}