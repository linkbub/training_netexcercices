using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Activitat4
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] arrayNoms = {"David", "Sergio", "Maria", "Laura", "Oscar", "Julia", "Oriol" };

            var nomsO =
                from noms in arrayNoms
                where noms.StartsWith("O")
                select noms;
            
            foreach(var nom in nomsO)
            {
                Console.Write(nom + " ");
            }

            Console.Write("\n");

            var nomsSis =
                from nomsS in arrayNoms
                where nomsS.Length > 5
                select nomsS;

            foreach (var nomSis in nomsSis)
            {
                Console.Write(nomSis + " ");
            }

            Console.Write("\n");

            var nomsOrdenats =
                from ordenats in arrayNoms
                orderby ordenats descending
                select ordenats;

            foreach (var nomsOrd in nomsOrdenats)
            {
                Console.Write(nomsOrd + "\n");
            }

            Console.Read();
        }
    }
}
