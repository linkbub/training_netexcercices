using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Activitat3
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] arrayNumeros = { 2, 6, 8, 4, 5, 5, 9, 2, 1, 8, 7, 5, 9, 6, 4 };

            var arrayMenors =
                (from petit in arrayNumeros
                where petit < 5
                select petit).ToArray();

            var arrayMajors =
                (from major in arrayNumeros
                 where major > 5
                 select major).ToArray();

            foreach(var num in arrayMenors)
            {
                Console.Write(num + " ");
            }

            Console.Write("\n");

            foreach (var num in arrayMajors)
            {
                Console.Write(num + " ");
            }

            Console.Read();
        }
    }
}
