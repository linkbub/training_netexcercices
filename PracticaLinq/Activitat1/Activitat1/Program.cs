using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Activitat1
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] arrayNumeros = {2, 6, 8, 4, 5, 5, 9, 2, 1, 8, 7, 5, 9, 6, 4 };
            int[] arrayParells;

            var contarParells =
                (from parells in arrayNumeros
                where (parells % 2) == 0
                select parells).ToArray();
            for (int i = 0; i < contarParells.Length; i++)
            {
                Console.Write(contarParells[i] + " ");
            }
            
            Console.Read();
        }
    }
}
