using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Activitat2
{
    class Program
    {
        const int maxCiutats = 6;

        static void Main(string[] args)
        {
            string bcn = "";
            string mdrd = "";
            string vlc = "";
            string mlg = "";
            string cad = "";
            string sant = "";

            string[] arrayCiutats = new string[maxCiutats];

            Console.WriteLine("Introdueix un nom de ciutat:");
            bcn = Console.ReadLine();
            Console.WriteLine("Introdueix un nom de ciutat:");
            mdrd = Console.ReadLine();
            Console.WriteLine("Introdueix un nom de ciutat:");
            vlc = Console.ReadLine();
            Console.WriteLine("Introdueix un nom de ciutat:");
            mlg = Console.ReadLine();
            Console.WriteLine("Introdueix un nom de ciutat:");
            cad = Console.ReadLine();
            Console.WriteLine("Introdueix un nom de ciutat:");
            sant = Console.ReadLine();

            arrayCiutats[0] = bcn;
            arrayCiutats[1] = mdrd;
            arrayCiutats[2] = vlc;
            arrayCiutats[3] = mlg;
            arrayCiutats[4] = cad;
            arrayCiutats[5] = sant;

            Array.Sort(arrayCiutats);
            Console.WriteLine("\n Ciutats ordenades per ordre alfabetic");
            

            for (int i = 0; i < maxCiutats; i++)
            {
                Console.WriteLine(arrayCiutats[i]);
            }

            Console.Read();
        }
    }
}
