using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Activitat4
{
    class Program
    {
        const int traspasCada = 4;

        static void Main(string[] args)
        {
            string nom = "Leo";
            string cognom1 = "Suarez";
            string cognom2 = "Puyol";
            string nomComplet = nom + " " + cognom1 + " " + cognom2;

            int dia = 25;
            int mes = 06;
            int any = 1989;
            string anyComplet = dia + "/" + mes + "/" + any;

            string anyCert = "L'any de naixement és de traspàs";
            string anyFals = "L'any de naixement no és de traspàs";

            int anyPrincipal = 1948;
            bool verificarAny = false;

            for (int i = anyPrincipal; i < any; i += traspasCada)
            {
                if (any == anyPrincipal)
                {
                    verificarAny = true;
                }
            };

            Console.Write("El meu nom és " + nomComplet + "\n");
            Console.Write("Vaig néixer el " + anyComplet + "\n");
            if (verificarAny == true)
            {
                Console.Write(anyCert);
            }
            else
            {
                Console.Write(anyFals);
            }

            Console.Read();
        }
    }
}
