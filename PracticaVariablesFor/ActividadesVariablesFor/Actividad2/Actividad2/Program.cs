using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Actividad2
{
    class Program
    {
        const int anyTraspas = 1948;
        const int traspasCada = 4;

        static void Main(string[] args)
        {
            int anyNaixement = 1989;
            int cantitatAnysTraspas = (anyNaixement - anyTraspas) / traspasCada;

            Console.Write(cantitatAnysTraspas);
            Console.Read();
        }
    }
}
