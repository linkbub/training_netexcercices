using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Activitat3
{
    class Program
    {
        static void Main(string[] args)
        {
            double puntsObtingutsPerPunt = 0;
            double puntsObtingutsPerActivitat = 0;
            double puntsObtingutsTotals = 0;
            int resposta;
            bool seguirActivitat = true;
            bool seguirSubActivitat = true;
            string nomActivitat = "";
            string nomSubActivitat = "";
            string nomPractica = "";
            

            Console.Write("Introdueix el nom de la practica a evaluar: \n");
            nomPractica = Console.ReadLine();

            while (seguirActivitat)
            {
                Console.Write("Introdueix el nom de l'activitat: \n");
                nomActivitat = Console.ReadLine();

                while (seguirSubActivitat)
                {
                    Console.Write("Introdueix el punt de l'activitat: \n");
                    nomSubActivitat = Console.ReadLine();

                    Console.Write("Introdueix la puntuacio obtinguda en aquest punt: \n");
                    puntsObtingutsPerPunt = double.Parse(Console.ReadLine());
                    
                    Console.Write("Voleu seguir afegint punts a l'activitat? (No:0 / Si:1) \n");
                    resposta = int.Parse(Console.ReadLine());

                    if (resposta == 1)
                    {
                        puntsObtingutsPerActivitat += puntsObtingutsPerPunt;
                    }
                    else if (resposta == 0)
                    {
                        puntsObtingutsPerActivitat += puntsObtingutsPerPunt;
                        seguirSubActivitat = false;
                    }
                }

                Console.Write("Voleu seguir afegint activitats? (No:0 / Si:1) \n");
                resposta = int.Parse(Console.ReadLine());

                if (resposta == 1)
                {
                    puntsObtingutsTotals += puntsObtingutsPerActivitat;
                    puntsObtingutsPerActivitat = 0;
                    seguirSubActivitat = true;
                }
                else if (resposta == 0)
                {
                    puntsObtingutsTotals += puntsObtingutsPerActivitat;
                    seguirActivitat = false;
                }
            }
            
            Console.Read();
        }
    }
}
