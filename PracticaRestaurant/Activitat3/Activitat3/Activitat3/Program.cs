﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Activitat3
{
    class Program
    {
        static void Main(string[] args)
        {
            
            int can, B500, B200, B100, B50, B20, B10, B5;
            B500 = 0;
            B200 = 0;
            B100 = 0;
            B50 = 0;
            B20 = 0;
            B10 = 0;
            B5 = 0;
            can = 0;

            int mesMenjar;
            bool seguir = true;
            bool menjarExisteix = false;
            string[] arrayMenu = new string[5];
            int[] arrayPreu = new int[5];

            List<string> listMenjar = new List<string>();

            for (int i = 0; i < arrayMenu.Length; i++)
            {
                Console.Write("Introdueix un plat: ");
                arrayMenu[i] = Console.ReadLine();
                Console.Write("Preu plat: ");
                arrayPreu[i] = int.Parse(Console.ReadLine());
            }

            for (int i = 0; i < arrayMenu.Length; i++)
            {
                Console.Write("Carta de menjas: \n");
                Console.Write(arrayMenu[i] + ": " + arrayPreu[i] + "€" + " \n");
            }

            while (seguir)
            {
                Console.Write("Que vols menjar? \n");
                listMenjar.Add(Console.ReadLine());
                Console.Write("Voleu demanar mes menjar? (1:Si / 0:No): ");
                mesMenjar = int.Parse(Console.ReadLine());

                if (mesMenjar == 0)
                {
                    seguir = false;
                }
            }

            foreach (var item in listMenjar)
            {
                for (int i = 0; i < arrayMenu.Length; i++)
                {
                    if (item == arrayMenu[i]) 
                    {
                        can += arrayPreu[i];
                        menjarExisteix = true;
                    }
                    
                }
                if (!menjarExisteix)
                {
                    Console.WriteLine("Aquest menjar no el tenim: " + item);
                }
                menjarExisteix = false;
            }
            Console.WriteLine("El preu total del teu menjar es: " + can);


            Console.Read();
        }
    }
}
